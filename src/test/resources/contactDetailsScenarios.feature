Feature: Demo Feature
  Background:
    Given Launch Home Page

  Scenario: Verify the Main office and Naperville office locations are correct on the “contact” page

    Given Contact details Page
    When Navigate to Contact details Page
    Then Validate the Contact details are correct
#    And Fill out the contact form (without actually submitting) on the “contact” page and take a screen shot of the filled form
    And Verify there is an open position with name “Senior Software Development Engineer In Test - Backend” is on the “Join our team” drop down
    And Navigate to the home page and find the opening for “Senior Software Development Engineer In Test - Backend”  click “Read More” and take a screenshot of the position.