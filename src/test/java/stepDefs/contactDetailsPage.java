package stepDefs;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import static com.codeborne.selenide.Selenide.*;

public class contactDetailsPage {
    @Given("Launch Home Page")
    public void https_www_physiq_com_contact() {
        // Write code here that turns the phrase above into concrete actions
        open("https://www.physiq.com");
    }

    @Given("Contact details Page")
    public void contact_details_page() {
        // Write code here that turns the phrase above into concrete actions
        open("https://www.physiq.com/contact/");
    }

    @When("Navigate to Contact details Page")
    public void navigate_to_contact_details_page() {
        // Write code here that turns the phrase above into concrete actions
        $("#navContactUs").shouldHave(Condition.text("Contact Us"));
    }

    @Then("Validate the Contact details are correct")
    public void validate_the_contact_details_are_correct() {
        // Write code here that turns the phrase above into concrete actions
        String mainOfficeContactDetails,nappervilleOfficeContactDetails;
        mainOfficeContactDetails = $x("//*[@id=\"contact\"]/section[2]/div/div/div[2]/p[1]").innerText();
        System.out.println("Main Office Contact Detail is: " + mainOfficeContactDetails);
        $x("//*[@id=\"contact\"]/section[2]/div/div/div[2]/p[1]").shouldHave(Condition.text("200 W. Jackson Blvd\n" +
                "\t\t\t\t\tSuite 550\n" +
                "\t\t\t\t\tChicago, Illinois\n" +
                "\t\t\t\t\t60606\n" +
                "\t\t\t\t\tUnited States\n" +
                "\t\t\t\t\tPhone: 800.516.7902\n" +
                "\t\t\t\t\tE-mail: info@physIQ.com"));

        nappervilleOfficeContactDetails = $x("//*[@id=\"contact\"]/section[2]/div/div/div[2]/p[2]").innerText();
        System.out.println("Main Office Contact Detail is: " + nappervilleOfficeContactDetails);
        $x("//*[@id=\"contact\"]/section[2]/div/div/div[2]/p[2]").shouldHave(Condition.text("300 E. 5th Avenue\n" +
                "\t\t\t\t\tSuite 105\n" +
                "\t\t\t\t\tNaperville, Illinois\n" +
                "\t\t\t\t\t60563\n" +
                "\t\t\t\t\tUnited States"));
    }

    @Then("Fill out the contact form \\(without actually submitting) on the “contact” page")
    public void fill_out_the_contact_form_without_actually_submitting_on_the_contact_page() {
        // Write code here that turns the phrase above into concrete actions
        // Entering Customer First
        $x("//*[@id=\"397113346\"]").click();
        sleep(5000);
        $("#field_397113346 > div").click();
        sleep(5000);
        $("#field_397113346 > div").setValue("Customer");
        // Entering Customer Last Name
        $x("//*[@id=\"397114370\"]").click();
        sleep(5000);
        $x("//*[@id=\"397114370\"]").setValue("Title");
        // Entering Customer Email Address
        $x("//*[@id=\"397113394\"]").click();
        $x("//*[@id=\"397113394\"]").setValue("customertitle@abcd.com");
        // Entering Customer Title
        $x("//*[@id=\"397117442\"]").click();
        $x("//*[@id=\"397117442\"]").setValue("Senior Software Engineer");
        // Entering Customer Company/Institution Name
        $x("//*[@id=\"397116418\"]").click();
        $x("//*[@id=\"397116418\"]").setValue("ABC Company");
        // Entering Customer Phone Number
        $x("//*[@id=\"397120514\"]").click();
        $x("//*[@id=\"397120514\"]").setValue("111-1111-111");
        screenshot("ContactDetails");
    }

    @Then("Verify there is an open position with name “Senior Software Development Engineer In Test - Backend” is on the “Join our team” drop down")
    public void verify_there_is_an_open_position_with_name_senior_software_development_engineer_in_test_backend_is_on_the_join_our_team_drop_down() {
        // Write code here that turns the phrase above into concrete actions
        sleep(2000);
        $("#navJoinOurTeam > a").hover();
        sleep(2000);
        $("#navSeniorSoftwareDevelopmentEngineerInTestBackend > a").shouldHave(Condition.text("Senior Software Development Engineer In Test - Backend"));
        sleep(2000);
        $("#main-header > div > nav > div.navbar-header > a > img").hover();
        sleep(2000);
    }

    @Then("Navigate to the home page and find the opening for “Senior Software Development Engineer In Test - Backend”  click “Read More” and take a screenshot of the position.")
    public void navigate_to_the_home_page_and_find_the_opening_for_senior_software_development_engineer_in_test_backend_click_read_more_and_take_a_screenshot_of_the_position() {
        // Write code here that turns the phrase above into concrete actions
        sleep(3000);
        $x("/html/body/section[2]/div/div/div[2]/p[4]/a").isEnabled();
        sleep(3000);
        $x("/html/body/section[2]/div/div/div[2]/p[4]/a").click();
        sleep(3000);
        $x("//*[@id=\"main-section\"]/div/h3[18]/a").shouldHave(Condition.text("Senior Software Development Engineer In Test - Backend"));
        sleep(3000);
        $("#main-section > div > p:nth-child(107) > a").click();
        sleep(3000);
        screenshot("readmorepage");
        sleep(5000);
    }
}